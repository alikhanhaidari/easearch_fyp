<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Metadata;

class scanning extends Controller
{
    public function scanning()
    {
        return view("listalldirectories");
    }

    public function openfile($path)
    {  
        header('Content-type:application/pdf');
        readfile(urldecode($path)); 
        // return redirect('scanning'); 
    }
    public function openfolder($path)
    { 
        $ext = pathinfo(urldecode($path));  

        chdir($ext['dirname']);
        exec("start .");
        // return redirect('scanning'); 
    }

    public function scanning_folders()
    {
        if (Input::has('path')) {
            $this->path = Input::get('path');   
            $con = array();
            $con = $this->getDirContents($this->path);
            
            $contents = array();
            foreach ($con as $entry) 
            {
                $ext = pathinfo($entry);
                if (isset($ext['extension']) && ($ext['extension'] == 'pdf' || $ext['extension'] == 'txt')) { 
                    $subcontents = [
                        "entity" => $entry,
                        "basename" => $ext['basename'],
                        "size" => (string) round((filesize($entry)/1024),2),
                        "type" => mime_content_type($entry),
                    ];
                    $contents[] = $subcontents;
                }
            } 
  
            return view("lists")->with('contents', $contents);
         }
         else
         {
            return view("lists");
         } 
    }

    public function getDirContents($dir)
    { 
    $handle = opendir($dir);
    if ( !$handle ) return array();
    $contents = array();
    while ( $entry = readdir($handle) )
    {
        if ( $entry=='.' || $entry=='..' ) continue;

        $entry = $dir.DIRECTORY_SEPARATOR.$entry;
        if ( is_file($entry) )
        {
            $contents[] = $entry;
        }
        else if ( is_dir($entry) )
        {
        $contents = array_merge($contents, $this->getDirContents($entry));
        }
    }
    closedir($handle);
    return $contents;
    }


    public function extracting($path)
    {
        $url = urldecode($path);
        // echo $url.'<br>';
        $parser = new \Smalot\PdfParser\Parser();
        $pdf    = $parser->parseFile($url); 
        $details  = $pdf->getDetails();

        $author = "";
        $title = "";
        $moddate = "";
        $creationdate = "";
        $pages = "";
        $language = "";
        $data = array();

        if(isset($details['Author'])) { $author = $details['Author']; }
        if(isset($details['Title']))  { $title = $details['Title']; }
        if(isset($details['ModDate']))  { $moddate = $details['ModDate']; }
        if(isset($details['CreationDate']))  { $creationdate = $details['CreationDate']; }
        if(isset($details['Pages']))  { $pages = $details['Pages']; }
        if(isset($details['Language']))  { $language = $details['Language']; }

        $data = [
            'Author' => $author,
            'Title' => $title,
            'ModDate' => date_format(date_create($moddate),"Y/m/d H:i:s"),
            'CreationDate' => date_format(date_create($creationdate),"Y/m/d H:i:s"),
            'Pages' => $pages,
            'Language' => $language,
            'url' => $url,
        ];

        return view("extract")->with('data', $data);

    }

    public function extracted()
    {
        if (Input::has('title') && Input::has('author') && Input::has('moddate') 
        && Input::has('creationdate') && Input::has('pages') && Input::has('language')&& Input::has('url')) {
            $title = Input::get('title'); 
            $author = Input::get('author');
            $moddate = Input::get('moddate');
            $creationdate = Input::get('creationdate');
            $pages = Input::get('pages');
            $language = Input::get('language');
            $url = Input::get('url'); 
            $ext = pathinfo($url);   
            $extension = $ext['extension'];
            $filename = $ext['filename'];
            $mime = mime_content_type($url);
            $size = round((filesize($url)/1024),2);


            // echo $title;
            // echo $author;
            // echo $moddate;
            // echo $creationdate;
            // echo $pages;
            // echo $language;
            // echo $url;
            // echo $ext['extension'];
            // echo $ext['filename'];
            // echo mime_content_type($url);
            // echo round((filesize($url)/1024),2);

            $md = new Metadata;
 
            $md->title = $title;
            $md->author = $author;
            $md->moddate = $moddate;
            $md->creationdate = $creationdate;
            $md->pages = $pages;
            $md->language = $language;
            $md->url = $url;
            $md->extension = $extension;
            $md->filename = $filename;
            $md->mime = $mime;
            $md->size = $size;

            // $md->save();
            

            try{ 
                $md->save(); 
                echo 'done';
             }
             catch(\Exception $e){ 
                // echo $e->getMessage();
                echo $e->getCode();
                // echo $e->getStatusCode();
                // echo $e->statusCode();
                echo 'this file exist already';
             }

            

        }else{
            echo "no data";
        }
    }
}



// foreach ($con as $entry) 
// {
// $filename = basename($entry);
// echo $filename."\n<br>";
// echo $entry."\n<br>";
// $ext = pathinfo($entry);
// $ext['dirname']
// $ext['filename']
// $ext['basename']
// $ext['extension']
// date("F d Y H:i:s.", filemtime($entry))
// filetype($entry)
// mime_content_type($entry)
// round((filesize($entry)/1024/1025),2)

// chdir($ext['dirname']);
// exec("start .");
// if (isset($ext['extension']) && ($ext['extension'] == 'pdf' || $ext['extension'] == 'txt')) { 
//     echo '<a href="'.$entry.'" target="_blank">'.$ext['basename'].'</a> || '.$ext['filename'].'
//      || '.round((filesize($entry)/1024/1025),2).' mb || '.filetype($entry).' || '.date("F d Y H:i:s.", filemtime($entry)).' <br>';
//    }


    
// }
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// use App\Task;
// use Illuminate\Http\Request;  

    Route::get('/', 'index@home');
    Route::get('/scanning', 'scanning@scanning');
    Route::post('/scanning1', 'scanning@scanning_folders');
    Route::get('/openfile/{path}', 'scanning@openfile');
    Route::get('/openfolder/{path}', 'scanning@openfolder');
    Route::get('/metadata/extract/{path}', 'scanning@extracting');
    Route::post('/metadata/extracted', 'scanning@extracted');

// Route::group(['middleware' => ['web']], function () {

    

//     /**
//      * Show Task Dashboard
//      */
//     Route::get('/', function () {
//         return view('tasks', [
//             'tasks' => Task::orderBy('created_at', 'asc')->get()
//         ]);
//     });

//     /**
//      * Add New Task
//      */
//     Route::post('/task', function (Request $request) {
//         $validator = Validator::make($request->all(), [
//             'name' => 'required|max:255',
//         ]);

//         if ($validator->fails()) {
//             return redirect('/')
//                 ->withInput()
//                 ->withErrors($validator);
//         }

//         $task = new Task;
//         $task->name = $request->name;
//         $task->save();

//         return redirect('/');
//     });

//     /**
//      * Delete Task
//      */
//     Route::delete('/task/{id}', function ($id) {
//         Task::findOrFail($id)->delete();

//         return redirect('/');
//     });
// });

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metadata extends Model
{
    

    protected $fillable = [
        'author', 'title', 'moddate','creationdate', 'pages', 'language', 'url', 'extension', 'filename', 'mime', 'size',
    ];

    protected $table = 'metadata';
}

@extends('layouts.app')

@section('content')
    <div class="container">
                <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 bg-secondary rounded py-5 mt-5 text-center">  
                {!! Form::open(array('url' => 'metadata/extracted','class'=>'form-inline','id'=>'loginform','role'=>'form', 'method'=>'post', 'enctype'=>'multipart/form-data')) !!}
                <div class="form-group my-3 col-md-6">
                <input type="text" class="form-control col-12" value="{{$data['Title']}}" name="title" placeholder="Title">
                </div>
                <div class="form-group my-3 col-md-6">
                <input type="text" class="form-control col-12" value="{{$data['Author']}}" name="author" placeholder="Author">
                </div> 
                <div class="form-group my-3 col-md-6">
                <input type="text" class="form-control col-12" value="{{$data['ModDate']}}" name="moddate" placeholder="Modification Date">
                </div>
                <div class="form-group my-3 col-md-6">
                <input type="text" class="form-control col-12" value="{{$data['CreationDate']}}" name="creationdate" placeholder="Creation Date">
                </div> 
                <div class="form-group my-3 col-md-6">
                <input type="text" class="form-control col-12" value="{{$data['Pages']}}" name="pages" placeholder="Pages">
                </div>
                <div class="form-group my-3 col-md-6">
                <input type="text" class="form-control col-12" value="{{$data['Language']}}" name="language" placeholder="Language">
                <input type="hidden" class="form-control col-12" value="{{$data['url']}}" name="url">
                </div> 
                <div class="form-group my-3 col-md-6"> 
                </div>
                <div class="form-group my-3 col-md-6">
                <button type="submit" class="btn btn-warning mb-2">Submit Metadata</button>
                </div>  
                {!! Form::close() !!} 
                </div>  
                </div> 
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
                <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 bg-secondary rounded py-5 mt-5">  
                <!-- <form class="form-inline">  -->
                {!! Form::open(array('url' => 'scanning1','class'=>'form-inline','id'=>'loginform','role'=>'form', 'method'=>'post', 'enctype'=>'multipart/form-data')) !!}
                <div class="form-group mx-sm-3 mb-2 col-7">
                <input type="text" class="form-control col-12" name="path" placeholder="Paste Folder Path">
                </div>
                <button type="submit" class="btn btn-warning mb-2"><i class="fa fa-search"></i> Start Scanning</button>
                <!-- </form> -->
                {!! Form::close() !!} 
                </div>  
                </div> 
    </div>
@endsection
